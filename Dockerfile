FROM nginx:stable
LABEL maintainer="palonder.roman5@gmail.com"
WORKDIR /usr/share/nginx/html
COPY ./index.html /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
EXPOSE 80
